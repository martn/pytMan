
SRCDIR = ./pytMan/
EXCDIR = ./scripts/
INSTOPT = --user


install: 
	python setup.py install ${INSTOPT}


tar : 
	find ${SRCDIR} -name "*.py" -type f -print0 | tar -cvf pytMan_`date +%Y%m%d`.tar --null -T - 
	tar -rvf pytMan_`date +%Y%m%d`.tar setup.py
	git status -u no > gitStatus.log ; git log -1 >> gitStatus.log
	tar -rvf pytMan_`date +%Y%m%d`.tar gitStatus.log
	rm gitStatus.log

.PHONY:  install tar

# vim: set noexpandtab noautoindent:
