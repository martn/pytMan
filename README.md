``pytMan`` - a very simple task manager
=======================================

``pytMan`` (python task manager) contains a class for managing and sequencing task launches.
It is build around the module [subprocess](https://docs.python.org/2/library/subprocess.html).

It is meant to assemble sequences of executables from within python scripts, checking the existence of of inputs and outputs before launching the executable.

Install
-------
This will install the module in your local home directory in your ``${PYTHONPATH}``
(under Ubuntu 14 it will install it ``~/.local/lib/python2.7/site-packages/``).
```
python setup.py install --user
```

Todo
----

* [x] infinite recall?  recall=-1?
* [x] Migrate to [subprocess32](https://docs.python.org/3.5/library/subprocess.html)
      (a backport of python 3 `subprocess`).
      This is a possible solution to [pyAS2#20](mad001/pyAS2#20)
* [ ] Possibility to lauch parallel tasks on processes 
* [ ] Unit testing + scenarios!
* [/] A more complete way to deal with list of outputs 
  (should all of them be there, only one trigger the Exception, etc.)
* [ ] Dealing with absolute path
* [ ] An initialiser from a command string parser
* [ ] Redirection of outputs and logging
