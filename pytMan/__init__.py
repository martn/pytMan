#-------------------------- LICENCE BEGIN ---------------------------
# This file is part of pytMan.
#
# pytMan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytMan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pytMan.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors - Martin Deshaies-Jacques
#
# Copyright 2016 - Air Quality Research Division, 
#                  Environnement And Climate Change Canada
#-------------------------- LICENCE END -----------------------------
'''
Simpler task manager

``pytMan`` (python task manager) contains a class for managing and sequencing task launches.
It is build around the module [subprocess](https://docs.python.org/2/library/subprocess.html).
'''

__version__ = 0.1
__author__ = 'Martin Deshaies-Jacques'
__copyright__ = 'Air Quality Research Division, Environment Canada'
__email__ = 'martin.deshaies-jacques@canada.ca'

__docformat__ = "restructuredtext"

__license__ = "GPL 3"

from taskManagerCls import TaskManager
