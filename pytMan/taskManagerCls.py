#-------------------------- LICENCE BEGIN ---------------------------
# This file is part of pytMan.
#
# pytMan is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pytMan is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pytMan.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors - Martin Deshaies-Jacques
#
# Copyright 2016 - Air Quality Research Division, 
#                  Environnement And Climate Change Canada
#-------------------------- LICENCE END -----------------------------
import os
from sys import version_info
if os.name == 'posix' and version_info[0] < 3:
    import subprocess32 as subprocess
else:
    raise RuntimeError('Cannot load subprocess32')
    #import subprocess
import sys

class FilesError(Exception):
    def __init__(self, files):
        self.files = files
    def __str__(self):
        return repr(self.files)
class NotExecFileError(Exception):
    def __init__(self, file):
        self.file = file
    def __str__(self):
        return repr(self.file)
    pass

class MissingFiles(FilesError):
    pass
class ExistingFiles(FilesError):
    pass
class MissingDirectories(FilesError):
    pass

class TaskManager(object):
    '''
    Task manager instance

    `TaskManager` instance need first to be instantiated before
    the task is launched.
    At instantations, are provided:

        * the executable
        * positional arguments
        * options without arguments
        * options with arguments
        * inputs
        * outputs

    The task so instantiated can then be launched using the 
    `__call__` special method, which takes some optionals arguments 
    controling its behavior.
    '''
    def __init__(   self, executable,
                    pos=list(), opt=list(), kwargs=dict(), 
                    outputs=list(), inputs=list(),
                    checkExec=False, 
                    title=None, verbose=False
                    ):
        '''
        :Parameters:
            executable : str
                The (full path) executable (must have the execution 
                permission)
            pos : list<str>
                Ordered positional arguments (will come first and in order
                when the command is builded)
            opt : list<str>
                Options without arguments
            kwargs : dict<str,*>
                Options wich takes arguments (the option is the key and
                the argument the value of the ``dict`` provided)
            outputs : list<str>
                Expected outputs (files) to be produced (used to determine 
                if the task has already been completed)
            inputs : list<str> | set(<str>)
                Required inputs (files) necessary to complet the task
            checkExec : bool
                If ``True``, will verify the execution permission of the
                executable
            title : None | str
                An optional title
            verbose : bool
                If ``True``, toggles verbosity of the manager
        '''

        self._executable = executable
        self._outputs = set(outputs)
        self._inputs = set(inputs)

        self._title = title
        self._verbose = verbose

        self._buildCommand(pos, opt, kwargs)
        if checkExec: self.checkExec()

    @property
    def outputs(self): return self._outputs
    @property
    def inputs(self): return self._inputs
    @property
    def title(self): return self._title

    def printTitle(self):
        if self._title != None:
            print('\n'+'='*50)
            print(self._title)
            print('='*50)

    def _buildCommand(self, pos, opt, kwargs):
        '''
        Builds the actual command to be passed to ``subprocess``.
        The command is built as a ``str`` composed first of the 
        executable, followed by positional arguments, simple options
        and then options with arguments.
        '''
        self._command = list()
        self._command.append(self._executable)
        for arg in pos:
            self._command.append(str(arg))
        for arg in opt:
            self._command.append(str(arg))
        for key, val in kwargs.iteritems():
            self._command.append(str(key))
            self._command.append(str(val))

    def __str__(self):
        out = ''
        for elem in self._command:
            out += elem + ' '
        return out

    def __call__(   self, 
                    exitOnFailure=False,
                    successOnExistAll=True, 
                    recall=False,
                    appendOutput=False, outputCheck=True, 
                    ignoreOutputDir=False, mkDirectory=True,
                    shell=False
                    ):
        '''
        Verifies the inputs and outputs, then if everything is fine, 
        launches the executable.

        :Parameters:
            exitOnFailure : bool
                If there is an error, the calling program will exit with the proper
                code, otherwise the exit code is returned to the calling program.
            successOnExistAll : bool
                Exits with success, but without executing the command, 
                when all outputs exist.
                Usefull to continue a sequence with intermediary outputs.
                already existing.
            recall : int
                False (0) or the number of times the task should be relaunched 
                if it fails.
                -1 (or any negative number) means infinite recalls.
            appendOutput : bool
                Will append to existing output (have precedence over 
                `successOnExistAll`). 
            outputCheck: bool
                perform output check prior to run
            ignoreOutputDir : bool
                Does not check if outputs directories exists (usefull when the
                executable creates these directories).
            mkDirectory : bool
                Will create missing output directories (only if 
                ``ignoreOutputDir == False``).
            shell : bool
                Use `shell=` option of the `subprocess` module.
                (https://docs.python.org/2/library/subprocess.html)
        '''

        doLaunch = True
        exitCode = 0
        self.printTitle()
        print

        if self._verbose: print(self)
        # -- checking outputs
        try:
            self.checkInputs()
        except MissingFiles as inst:
            mesg = "Input files missing:\n" 
            for file in inst.files:
                mesg += '%s\n'%file
            if self._verbose: print(mesg)
            exitCode += 10
            doLaunch = False

        # -- checking outputs
        try:
            if outputCheck:
                self.checkOutputs()
        except ExistingFiles as inst:
            ''' All output files exist'''
            mesg = "All output files exist."
            if self._verbose: print(mesg)
            if appendOutput:
                if self._verbose: print("Appending.")
            elif successOnExistAll:
                if self._verbose: print("Continuing.")
                doLaunch = False
            else:
                if self._verbose: print("Delete and rerun.")
                exitCode += 100
                doLaunch = False
        except MissingFiles as inst:
            ''' Some output files exist, but not all of them'''
            mesg = "Some output files are missing, maybe something went wrong?\n"
            mesg += "Missing files:\n"
            for file in inst.files:
                mesg += '%s\n'%file
            if self._verbose: print(mesg)
            exitCode += 1000
            doLaunch = False
        except MissingDirectories as inst:
            ''' Some output directories are missing'''
            if not ignoreOutputDir:
                ''' they should be created!'''
                if mkDirectory:
                    ''' The manager is allowed to created them, and does so'''
                    for directory in inst.files:
                        if self._verbose:
                            print("Creating %s directory"%directory)
                        os.makedirs(directory)
                else:
                    ''' the manager is not allowed to create them: error!'''
                    exitCode += 10000
                    doLaunch = False
            
        '''
        make sure to flush the buffer before launching the task
        otherwise the subprocess outputs will come before when there is
        redirection see [issue #1](https://gitlab.science.gc.ca/mad001/pytMan/issues/1)
        '''
        sys.stdout.flush()
        
        nLaunches = 0
        pXCode = 0
        while doLaunch:

            ''' everything is fine, proceed with the launch''' 
            nLaunches += 1 
            if shell:
                shellCommand = self.__str__()
                pXCode = subprocess.call(shellCommand, shell=True)
            else:
                pXCode = subprocess.call(self._command, shell=False)

            if pXCode:
                '''
                If the call returns an error and recall is True
                decrement `recall` till it reaches False (0).

                If recall is negative then there will 
                be infinite recall since negative int are True.
                '''
                doLaunch = recall
                if recall > 0: 
                    print('\n'+'<!>'*3 + ' SUBPROCESS CALL ERROR: %d'%pXCode)
                    print(' '*9 + ' RECALLING (%d)\n%s\n'%(
                                        recall, self.__str__()))
                    recall -= 1
                
            else:
                break

        exitCode += pXCode

        if exitCode and exitOnFailure:
            ''' there was an error, abort and exit to the OS!'''
            sys.exit(exitCode)
        return exitCode


    def checkExec(self):
        '''
        Checks if the executable has the execution permission
        '''
        if not os.path.isfile(self._executable):
            raise MissingFiles([self._executable])
        if not os.access(self._executable, os.X_OK):
            raise NotExecFileError(self._executable)

    def checkInputs(self):
        '''
        Checks if all files in the inputs exist.
        If not, raises `MissingFiles` with the missing
        files as argument.
        '''
        files = list()
        for file in self._inputs:
            if not os.path.isfile(file):
                files.append(file)
        if len(files): raise MissingFiles(files)

    def checkOutputs(self):
        '''
        Check if outputs exist before the launch.
        For each output file, checks first if its directory exists;
        if not, logs the missing directory.
        Then checks if the file exists and log the existing file.

        * If there is no existing file, then all is good!
        * If all output files are present, raises `ExistingFiles` with 
          the logged existing files as argument;
        * Otherwise, raises `MissingFiles` with logged existing files
          as argument.

        If the are some missing directories, raises `MissingDirectories`
        with the logged missing output directories as argument.
        '''
        files = list()
        missingDirs = set()
        for file in self._outputs:
            dirname = os.path.dirname(file)
            if not os.path.isdir(dirname):
                missingDirs.add(dirname)
            if os.path.isfile(file):
                files.append(file)
        if len(files) == 0:
            pass
        elif len(files) == len(self._outputs):
            raise ExistingFiles(self._outputs)
        else:
            missing = self._outputs.difference(files)
            raise MissingFiles(missing)
        if len(missingDirs):
            raise MissingDirectories(missingDirs)

if __name__ == '__main__':
    
    from sys import exit

    inputs = ['in.txt']
    filename = 'directory/test.txt'
    cmd = TaskManager(  'touch', pos=[filename], opt=['-a'], 
                        outputs=[filename], inputs=inputs, 
                        verbose=True)
    exit(cmd())
    
