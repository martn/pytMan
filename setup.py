from distutils.core import setup
setup(name='pytMan',
      version='0.1',
      description='Simple task manager',
      author='Martin Deshaies-Jacques',
      author_email='martin.deshaies-jacques@ec.gc.ca',
      packages=['pytMan',]
      )
